<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interview;
use App\Candidate;
use App\User;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class InterviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates = Candidate::all();
        $interviews = Interview::all();
        return view('Interviews.index', compact('candidates','interviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('new-interview');

        $candidates = Candidate::all();
        $interviews = Interview::all();
        $users = User::all();
        return view('interviews.create',compact('candidates','interviews','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interview = new Interview();
        $inter = $interview->create($request->all());
        $inter->save();
        return redirect('interviews');
    }
    public function changeCandidate($iid, $cid)
    {
        $candidate = Candidate::findOrFail($cid);
        $interview = Interview::findOrFail($iid);
        $interview->candidate_id = $cid;
        $candidate->save();
        return back();
        //return redirect('candidates');
    }
    public function myInterviews()
    {
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $interviews = $user->interview;
        $users = User::all();
        return view('Interviews.index', compact('interviews','users'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
