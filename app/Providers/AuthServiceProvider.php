<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('new-interview', function ($user) {
            if($user->isManager() || $user->isAdmin()){
                return TRUE;
            }
            return FALSE;
        });
        Gate::define('change-status', function ($user, $candidate) {
            return $user->id === $candidate->user_id;
        });

        Gate::define('assign-user', function ($user) {
            return $user->isManager();
        });

        //
    }
}
