@extends('layouts.app')

@section('title', 'Interviews')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>
<h1>List of interview</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Content</th><th>Candidate</th><th>Interviewer</th><th>Date</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @if(count($interviews)!=0)
    @foreach($interviews as $interview)
        <tr>
            <td>{{$interview->id}}</td>
            <td>{{$interview->content}}</td>
            <td>
            @if(isset($interview->candidate_id))
            {{$interview->candidate->name}}
            @else
            Assign candidate
            @endif
            </td>
            </td>
            <td>
            @if(isset($interview->user_id))
            {{$interview->user->name}}
            @else
            Assign interviewer
            @endif
            </td>
            <td>{{$interview->date}}</td>
            <td>{{$interview->created_at}}</td>
            <td>{{$interview->updated_at}}</td>
        </tr>
    @endforeach
    @else
    <td>אין עדיין ראיונות</td>
    @endif

</table>
@endsection

