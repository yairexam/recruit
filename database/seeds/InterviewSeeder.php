<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class InterviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interviews')->insert([[
            'content' => Str::random(1000),
            'date' => Carbon::now(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ],
        [
            'content' => Str::random(1000),
            'date' => Carbon::now(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]]


    );
    }
}
